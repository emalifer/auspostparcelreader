// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const SETTIPS = 'window/SETTIPS';
export const SHOWHIDETIPS = 'window/SHOWHIDETIPS';

export function setTips(tips) {
  return {
    type: SETTIPS,
    tips
  };
}

export function showHideTips(show) {
  return {
    type: SHOWHIDETIPS,
    show
  };
}

export const SHOWVIRTUALKEYBOARD = 'window/SHOWVIRTUALKEYBOARD';

export const showHideVirtualKeyboard = (show) => {
  return dispatch => {
    dispatch({
      type: SHOWVIRTUALKEYBOARD,
      show
    });
  };
}

export const TOUCHSOUND = 'window/TOUCHSOUND';

export const touchSound = () => {
  return dispatch => {

    dispatch({
      type: TOUCHSOUND,
      touchSound: true
    });

    setTimeout(() => {
      dispatch({
        type: TOUCHSOUND,
        touchSound: false
      });
    }, 300)
  };
}