// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const SETPARCELFIELDVALUE = 'parcel/SETPARCELFIELDVALUE';

export function setParcelFieldValue(field, value) {
  return {
    type: SETPARCELFIELDVALUE,
    field,
    value
  };
}

export const SETDELIVERYFIELDVALUE = 'parcel/SETDELIVERYFIELDVALUE';

export function setDeliveryFieldValue(field, value) {
  return {
    type: SETDELIVERYFIELDVALUE,
    field,
    value
  };
}

export const CLEARPARCEL = 'parcel/CLEARPARCEL';

export function clearParcel() {
  return {
    type: CLEARPARCEL
  }
}