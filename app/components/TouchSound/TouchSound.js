import React, { Component } from 'react';

import sound from './click.mp3';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class TouchSound extends Component {

  constructor(props) {
    super(props);
    this.audio = new Audio(sound)
  }

  componentDidUpdate = (prevProps) => {
    if(this.props.touchSound && this.props.touchSound !== prevProps.touchSound){
      this.audio.play();
    }
  }

  render () {
    return (
      <></>
    )
  }
}

const mapStateToProps = state => ({
  touchSound: state.window.touchSound
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
    },
    dispatch
  );
  

export default connect(mapStateToProps, mapDispatchToProps)(TouchSound);