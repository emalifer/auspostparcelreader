// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import produce from 'immer';

import routes from '../../../constants/routes';
import styles from './ParcelSummary.css';
import * as WindowActions from '../../../actions/window';
import * as ParcelActions from '../../../actions/parcel';
import KeyboardComp from '../../KeyboardComp/KeyboardComp';

type Props = {};

class ParcelSummary extends Component<Props> {
  props: Props;

  state = {
  }

  componentDidMount = () => {

    this.props.setTips(
      [
      ]
    );

    document.addEventListener('keyup', this.tipsEvent, false);

  }

  tipsEvent = (event) => {
    const { key } = event;
    switch(key) {
      case '!':
        break;
      default:
        break;
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
  }

  updateField = (e, field) => {
    let val = e.target.value;
    this.props.setDeliveryFieldValue(field, val);
  }

  showKeyboard = () => {
    this.props.touchSound();
    this.props.showHideVirtualKeyboard(true);
  }

  componentWillUnmount = () => {
    document.removeEventListener('keyup', this.tipsEvent);
    this.props.setTips([]);
  }

  render() {

    // const { hasParcel, scanning, parcelInfoError } = this.state;
    const { parcel, delivery } = this.props;

    return (
      <div id={styles.parcelsummary}>

        <div className="p-header d-flex flex-column justify-content-end">
          <h1 className="text-center">Parcel Summary</h1>
        </div>
        
        <div className="d-flex flex-column position-relative" style={{flex: 1}}>
          <form className="d-block p-5" style={{flex: 1, overflow: 'auto'}}>
            <div className="container-fluid">
              <div className="row justify-content-center">
                <div className="col-10">

                  <div className="card mb-5">
                    <div className="card-header">
                      <h2 className="font-weight-bold mb-0" style={{lineHeight: '3rem'}}>Summary</h2>
                    </div>
                    <div className="card-body">
                    <table className="table mb-0 table-borderless">
                        <tbody>
                          <tr>
                            <td scope="col" width="250">Package Type</td>
                            <th scope="col">
                              {parcel.packageType ? parcel.packageType : <div className="skeleton" style={{width: '100px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Packaging</td>
                            <th scope="col">
                              {parcel.packaging ? parcel.packaging : <div className="skeleton" style={{width: '200px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Size</td>
                            <th scope="col">
                              {parcel.size ? parcel.size : <div className="skeleton" style={{width: '400px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Weight</td>
                            <th scope="col">
                              {parcel.weight ? parcel.weight : <div className="skeleton" style={{width: '200px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                        </tbody>
                      </table>
                      <hr/>
                      <table className="table mb-0 table-borderless">
                        <tbody>
                          <tr>
                            <td scope="col" width="250">Contact Name</td>
                            <th scope="col">
                              {delivery.contactName ? delivery.contactName : <div className="skeleton" style={{width: '300px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Mobile Number</td>
                            <th scope="col">
                              {delivery.mobileNumber ? delivery.mobileNumber : <div className="skeleton" style={{width: '100px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Address</td>
                            <th scope="col">
                              {delivery.POBoxStreet ? delivery.POBoxStreet : ``} <br/>
                              {delivery.suburbTown ? delivery.suburbTown : ``} &nbsp;
                              {delivery.state ? delivery.state : ``} &nbsp;
                              {delivery.postCode ? delivery.postCode : ``}
                            </th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body">
                      <table className="table mb-0 table-borderless">
                        <tbody>
                          <tr>
                            <td scope="col"><h2>Subtotal</h2></td>
                            <th scope="col">
                              <h2 className="text-right font-weight-bold">$ 9.90</h2>
                            </th>
                          </tr>
                          <tr>
                            <td scope="col">Included GST</td>
                            <th scope="col" className="text-right">
                              $ 9.90
                            </th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </form>

          <div className="d-flex flex-row">
            <NavLink to={routes.APP_DELIVERYINFO} className="btn btn-xxlg btn-auspostdred rounded-0" onClick={() => {this.props.touchSound();}} style={{flex: 1}}>Back</NavLink>
            <NavLink to={routes.APP_PARCELSUMMARY} className="btn btn-xxlg btn-auspostred rounded-0 disabled" onClick={() => {this.props.touchSound();}} style={{flex: 3}}>Tap to proceed (disabled)</NavLink>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    showVirtualKeyboard: state.window.showVirtualKeyboard,
    parcel: state.parcel.parcel,
    delivery: state.parcel.delivery
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setTips: WindowActions.setTips,
    showHideVirtualKeyboard: WindowActions.showHideVirtualKeyboard,
    setParcelFieldValue: ParcelActions.setParcelFieldValue,
    setDeliveryFieldValue: ParcelActions.setDeliveryFieldValue,
    clearParcel: ParcelActions.clearParcel,
    touchSound: WindowActions.touchSound
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ParcelSummary);
