// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { push } from 'connected-react-router';
import $ from 'jquery'; 
import 'bootstrap/dist/js/bootstrap.bundle';
import produce from 'immer';

import routes from '../../../constants/routes';
import styles from './Scan.css';
import * as WindowActions from '../../../actions/window';
import * as ParcelActions from '../../../actions/parcel';
import Camera from './Camera/Camera'

type Props = {};

const sampleParcelValue = {
  parcel: {
    packageType: 'PARCEL',
    packaging: 'SMALL SATCHEL',
    size: '220mm x 350mm',
    weight: '500g'
  },
  delivery: {
    company: 'LOREM IPSUM',
    contactName: 'JOHN DELA LEE',
    mobileNumber: '+61 894 3422 32',
    APCustomerNumber: 'AP4214',
    POBoxStreet: 'SUITE 10A 11 HINKLEY ROAD',
    suburbTown: 'DAWES POINT',
    state: 'NSW',
    postCode: '2000',
  }
}

class Scan extends Component<Props> {
  props: Props;

  state = {
    hasParcel: true,
    scanning: true,
    parcelInfoError: {
      errorMsg: 'Please place parcel with form facing up.',
      error: false
    },
    isFinishedScanning: false,
    willProceed: 0
  }

  sampleTimeouts = [];

  componentDidMount = () => {
    this.props.clearParcel();
    this.setTips();

    document.addEventListener('keyup', this.tipsEvent, false);

    this.runSampleScan();
    this.showHideParcelInfoErrorModal();
  }

  setTips = () => {
    this.props.setTips(
      [
        'Press ! to toggle simulate add/remove sample parcel on/from the scale and starts/stops scan',
        'Press @ to simulate error on parcel info',
        `Press # to simulate error on delivery info after scanning (press ! to restart scan) ${this.state.willProceed === -1 ? `*` : ``}`,
        `Press $ to simulate success after scanning (press ! to restart scan) ${this.state.willProceed === 1 ? `*` : ``}`,
        `Press % to no event after scanning (press ! to restart scan) ${this.state.willProceed === 0 ? `*` : ``}`,
      ]
    );
  }

  tipsEvent = (event) => {
    const { key } = event;
    switch(key) {
      case '!':
        this.clearSampleTimeouts();
        this.setState({hasParcel: !this.state.hasParcel}, () => {
          if(!this.state.hasParcel) {
            this.clearSampleScan();
            this.setState({scanning: false, isFinishedScanning: true});
          } else if(this.state.hasParcel) {
            this.setState({scanning: true, isFinishedScanning: false});
          }
        });
        break;
      case '@':
        this.setState(produce(draft => {
          draft.parcelInfoError.error = !this.state.parcelInfoError.error
        }));
        break;
      case '#':
        this.setState({willProceed: -1}, () => {
          this.setTips();
        });
        break;
      case '$':
        this.setState({willProceed: 1}, () => {
          this.setTips();
        });
        break;
      case '%':
        this.setState({willProceed: 0}, () => {
          this.setTips();
        });
        break;
      default:
        break;
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(prevState.scanning !== this.state.scanning && this.state.scanning) {
      this.clearSampleScan();
      this.runSampleScan();
    }
    if(prevState.parcelInfoError !== this.state.parcelInfoError) {
      this.showHideParcelInfoErrorModal();
    }
    if(prevState.isFinishedScanning !== this.state.isFinishedScanning && this.state.isFinishedScanning) {
      this.checkParcelInfo();
    }
  }

  runSampleScan = () => {
    this.sampleTimeouts.push(setTimeout(() => { this.props.setParcelFieldValue('packageType', sampleParcelValue.parcel.packageType);}, Math.random() * 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setParcelFieldValue('packaging', sampleParcelValue.parcel.packaging);}, Math.random() * 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setParcelFieldValue('size', sampleParcelValue.parcel.size);}, Math.random() * 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setParcelFieldValue('weight', sampleParcelValue.parcel.weight);}, Math.random() * 5000));

    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('company', sampleParcelValue.delivery.company);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('contactName', sampleParcelValue.delivery.contactName);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('mobileNumber', sampleParcelValue.delivery.mobileNumber);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('APCustomerNumber', sampleParcelValue.delivery.APCustomerNumber);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('POBoxStreet', sampleParcelValue.delivery.POBoxStreet);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('suburbTown', sampleParcelValue.delivery.suburbTown);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('state', sampleParcelValue.delivery.state);}, Math.random() * 5000 + 5000));
    this.sampleTimeouts.push(setTimeout(() => { this.props.setDeliveryFieldValue('postCode', sampleParcelValue.delivery.postCode);}, Math.random() * 5000 + 5000));

    
    this.sampleTimeouts.push(setTimeout(() => { this.setState({isFinishedScanning: true});}, 10000));
  }

  clearSampleScan = () => {
    this.props.clearParcel();
  }

  clearSampleTimeouts = () => {
    this.sampleTimeouts.forEach((sampleTimeout) => {
      clearTimeout(sampleTimeout);
    });
  }

  showHideParcelInfoErrorModal = () => {
    if(this.state.parcelInfoError.error){
      $('#parcelInfoErrorModal').modal('show');
      $('.modal-backdrop').appendTo($('#parcelInfoErrorModal').parent());
      $('body').removeClass('modal-open');

      $('#parcelInfoErrorModal').on('hidden.bs.modal', (e) => {
        this.setState(produce(draft => {
          draft.parcelInfoError.error = false;
        }));
      })
    }
  }

  checkParcelInfo = () => {
    switch(this.state.willProceed) {
      case -1:
        this.props.touchSound();
        this.props.push(routes.APP_DELIVERYINFO);
        break;
      case 1:
        this.props.touchSound();
        this.props.push(routes.APP_PARCELSUMMARY);
        break;
      case 0:
      default:
        break;
    }
  }

  componentWillUnmount = () => {
    document.removeEventListener('keyup', this.tipsEvent);
    this.props.setTips([]);
    this.clearSampleTimeouts();
  }

  render() {

    const { hasParcel, scanning, parcelInfoError } = this.state;
    const { parcel, delivery } = this.props;

    return (
      <div id={styles.scan}>
        <Camera scanning={scanning} hasParcel={hasParcel} />
        
        <div className="d-flex flex-column position-relative" style={{flex: 1}}>
          <form className="d-block p-5" style={{flex: 1}}>
            <div className="container-fluid">
              <div className="row justify-content-center">
                <div className="col-10">
                  <div className="card">
                    <div className="card-body">
                      <table className="table mb-0 table-borderless">
                        <tbody>
                          <tr>
                            <td scope="col" width="250">Package Type</td>
                            <th scope="col">
                              {parcel.packageType ? parcel.packageType : <div className="skeleton" style={{width: '100px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Packaging</td>
                            <th scope="col">
                              {parcel.packaging ? parcel.packaging : <div className="skeleton" style={{width: '200px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Size</td>
                            <th scope="col">
                              {parcel.size ? parcel.size : <div className="skeleton" style={{width: '400px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Weight</td>
                            <th scope="col">
                              {parcel.weight ? parcel.weight : <div className="skeleton" style={{width: '200px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                        </tbody>
                      </table>
                      <hr/>
                      <table className="table mb-0 table-borderless">
                        <tbody>
                          <tr>
                            <td scope="col" width="250">Company</td>
                            <th scope="col">
                              {delivery.company ? delivery.company : <div className="skeleton" style={{width: '300px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Contact Name</td>
                            <th scope="col">
                              {delivery.contactName ? delivery.contactName : <div className="skeleton" style={{width: '300px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Mobile Number</td>
                            <th scope="col">
                              {delivery.mobileNumber ? delivery.mobileNumber : <div className="skeleton" style={{width: '100px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">AP Cust. No.</td>
                            <th scope="col">
                              {delivery.APCustomerNumber ? delivery.APCustomerNumber : <div className="skeleton" style={{width: '100px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">PO Box / Street</td>
                            <th scope="col">
                              {delivery.POBoxStreet ? delivery.POBoxStreet : <div className="skeleton" style={{width: '300px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Suburb / Town</td>
                            <th scope="col">
                              {delivery.suburbTown ? delivery.suburbTown : <div className="skeleton" style={{width: '200px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">State</td>
                            <th scope="col">
                              {delivery.state ? delivery.state : <div className="skeleton" style={{width: '50px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                          <tr>
                            <td scope="col" width="250">Postcode</td>
                            <th scope="col">
                              {delivery.postCode ? delivery.postCode : <div className="skeleton" style={{width: '60px'}}>&nbsp;</div>}
                            </th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

          <div id="parcelInfoErrorModal" className="modal fade position-absolute" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" style={{fontSize: '2rem'}}>Error: Parcel Info</h5>
                </div>
                <div className="modal-body pt-5 pb-5">
                  <div className="container-fluid">
                    <div className="row justify-content-center">
                      <div className="col-4 text-center">
                        <i className="fas fa-exclamation-circle" style={{fontSize: '8rem'}}></i>
                      </div>
                      <div className="col-8">
                        <p style={{fontSize: '3rem'}}>{parcelInfoError.errorMsg}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-block btn-xlg btn-auspostred rounded-0 font-weight-bold" data-dismiss="modal">Tap to proceed</button>
                </div>
              </div>
            </div>
          </div>

          <NavLink to={routes.APP_ATTRACTLOOP} className="btn btn-block btn-xxlg btn-auspostred rounded-0" onClick={() => {this.props.touchSound();}}>Back</NavLink>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    parcel: state.parcel.parcel,
    delivery: state.parcel.delivery
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setTips: WindowActions.setTips,
    setParcelFieldValue: ParcelActions.setParcelFieldValue,
    setDeliveryFieldValue: ParcelActions.setDeliveryFieldValue,
    clearParcel: ParcelActions.clearParcel,
    touchSound: WindowActions.touchSound,
    push
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Scan);
