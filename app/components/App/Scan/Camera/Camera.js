// @flow
import React, { Component } from 'react';

import styles from './Camera.css';
import sampleparcel from './sampleparcel.png';

export default class Camera extends Component<Props> {
  props: Props;

  render() {
    return (
      <div id={styles.camera}>
        {this.props.hasParcel &&
          <img src={sampleparcel} alt="Sample Parcel" />
        }
        <div id={styles.prompt}>
          <h1 className={`text-center text-white ${this.props.scanning ? styles[`scanning`] : ``}`}>
            {this.props.scanning ? <span>Scanning</span> : <span>Place your parcel<br/>on the scale</span>}
          </h1>
        </div>
      </div>
    );
  }
}