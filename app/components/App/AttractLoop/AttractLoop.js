// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import routes from '../../../constants/routes';
import styles from './AttractLoop.css';
import * as WindowActions from '../../../actions/window';
import logo from '../../../assets/images/logo.png';
import banner from './banner.png';

type Props = {};

class AttractLoop extends Component<Props> {
  props: Props;

  componentDidMount = () => {
    this.props.setTips(
      ['Press ! to simulate place sample parcel']
    );

    document.addEventListener('keyup', this.placeParcel, false);  
  }
  

  placeParcel = (event) => {
    const { key } = event;
    if(key === '!') {
      this.props.touchSound();
      this.props.push(routes.APP_SCAN);
    }
  }

  componentWillUnmount = () => {
    document.removeEventListener('keyup', this.placeParcel);
    this.props.setTips([]);
  }

  render() {

    return (
      <div id={styles.attractloop}>
        <img id={styles.logo} src={logo} alt="Australia Post" />
        <div id={styles.banner}>
          <img src={banner} alt="Banner" />
        </div>
        <div id={styles.prompt}>
          <h1 className="text-center">
            Place your parcel<br/>
            on the scale</h1>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setTips: WindowActions.setTips,
    touchSound: WindowActions.touchSound,
    push
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AttractLoop);
