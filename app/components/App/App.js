// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Switch, Route } from 'react-router';

import routes from '../../constants/routes';
import styles from './App.css';
import * as WindowActions from '../../actions/window';
import TouchSound from '../TouchSound/TouchSound';
import AttractLoop from './AttractLoop/AttractLoop';
import Scan from './Scan/Scan';
import DeliveryInfo from './DeliveryInfo/DeliveryInfo';
import ParcelSummary from './ParcelSummary/ParcelSummary';

import { productName, version } from '../../../package.json';

type Props = {};

class App extends Component<Props> {
  props: Props;

  componentDidMount() {
    document.addEventListener('keyup', this.showHideTips, false);    
  }

  showHideTips = (event) => {
    const { key } = event;
    if(key === '+') {
      this.props.showTips ? this.props.showHideTips(false) : this.props.showHideTips(true);
    }
  }

  componentWillUnmount = (event) => {
    document.removeEventListener('keyup', this.showHideTips);
  }

  render() {

    const { showTips, tips } = this.props;

    return (
      <div id="app">

        {showTips &&
          <div id={styles.devSwitches} className="pt-3 pb-1 pl-3 pr-3">
            <h4>Switches:</h4>
            {tips.length > 0 &&
              <ul className="pl-3">
                {tips.map((tip, idx) => {
                  return (<li key={idx}>{tip}</li>);
                })}
              </ul>
            }
            {tips.length === 0 &&
              <h5>No tips for this page</h5>
            }
          </div>
        }

        <Switch>
          <Route path={routes.APP_ATTRACTLOOP} component={AttractLoop} exact />
          <Route path={routes.APP_SCAN} component={Scan} exact />
          <Route path={routes.APP_DELIVERYINFO} component={DeliveryInfo} exact />
          <Route path={routes.APP_PARCELSUMMARY} component={ParcelSummary} exact />
        </Switch>
        <TouchSound />
        <div id={styles.devInfo} className="pt-1 pb-1 pl-2 pr-2">{productName} POC {version} Development Build, Press + to toggle show/hide switches</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    tips: state.window.tips,
    showTips: state.window.showTips
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    showHideTips: WindowActions.showHideTips
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
