// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import produce from 'immer';

import routes from '../../../constants/routes';
import styles from './DeliveryInfo.css';
import * as WindowActions from '../../../actions/window';
import * as ParcelActions from '../../../actions/parcel';
import KeyboardComp from '../../KeyboardComp/KeyboardComp';

type Props = {};

class DeliveryInfo extends Component<Props> {
  props: Props;

  state = {
  }

  componentDidMount = () => {

    this.props.setTips(
      [
        'Press ! to toggle virtual keyboard visibility',
      ]
    );

    document.addEventListener('keyup', this.tipsEvent, false);

  }

  tipsEvent = (event) => {
    const { key } = event;
    switch(key) {
      case '!':
        this.props.showHideVirtualKeyboard(!this.props.showVirtualKeyboard);
        break;
      default:
        break;
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
  }

  updateField = (e, field) => {
    let val = e.target.value;
    this.props.setDeliveryFieldValue(field, val);
  }

  showKeyboard = () => {
    this.props.touchSound();
    this.props.showHideVirtualKeyboard(true);
  }

  componentWillUnmount = () => {
    document.removeEventListener('keyup', this.tipsEvent);
    this.props.setTips([]);
  }

  render() {

    // const { hasParcel, scanning, parcelInfoError } = this.state;
    const { parcel, delivery } = this.props;

    return (
      <div id={styles.deliveryinfo}>

        <div className="p-header d-flex flex-column justify-content-end">
          <h1 className="text-center">Delivery Form</h1>
        </div>
        
        <div className="d-flex flex-column position-relative" style={{flex: 1}}>
          <form className="d-block p-5" style={{flex: 1, overflow: 'auto'}}>
            <div className="container-fluid">
              <div className="row justify-content-center">
                <div className="col-10">
                  <div className="card">
                    <div className="card-body">
                      <table className="table mb-0 table-borderless">
                        <tbody>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryCompany">Company</label>
                                <input id="deliveryCompany" className="form-control form-control-lg" type="text" defaultValue={delivery.company ? delivery.company : ``} placeholder="Company" onChange={(e) => { this.updateField(e, 'company');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryContactName">Contact Name</label>
                                <input id="deliveryContactName" className="form-control form-control-lg" type="text" defaultValue={delivery.contactName ? delivery.contactName : ``} placeholder="Contact Name" onChange={(e) => { this.updateField(e, 'contactName');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryMobileNumber">Mobile Number</label>
                                <input id="deliveryMobileNumber" className="form-control form-control-lg" type="text" defaultValue={delivery.mobileNumber ? delivery.mobileNumber : ``} placeholder="Mobile Number" onChange={(e) => { this.updateField(e, 'mobileNumber');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryContactName">AP Cust. No.</label>
                                <input id="deliveryAPCustomerNumber" className="form-control form-control-lg" type="text" defaultValue={delivery.APCustomerNumber ? delivery.APCustomerNumber : ``} placeholder="APCustomerNumber" onChange={(e) => { this.updateField(e, 'APCustomerNumber');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryPOBoxStreet">PO Box / Street</label>
                                <input id="deliveryPOBoxStreet" className="form-control form-control-lg" type="text" defaultValue={delivery.POBoxStreet ? delivery.POBoxStreet : ``} placeholder="PO Box / Street" onChange={(e) => { this.updateField(e, 'POBoxStreet');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliverySuburbTown">Suburb / Town</label>
                                <input id="deliverySuburbTown" className="form-control form-control-lg" type="text" defaultValue={delivery.suburbTown ? delivery.suburbTown : ``} placeholder="Suburb / Town" onChange={(e) => { this.updateField(e, 'suburbTown');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryState">State</label>
                                <input id="deliveryState" className="form-control form-control-lg" type="text" defaultValue={delivery.state ? delivery.state : ``} placeholder="State" onChange={(e) => { this.updateField(e, 'state');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th scope="col">
                              <div className="form-group">
                                <label htmlFor="deliveryPostCode">Postcode</label>
                                <input id="deliveryPostCode" className="form-control form-control-lg" type="text" defaultValue={delivery.postCode ? delivery.postCode : ``} placeholder="Postcode" onChange={(e) => { this.updateField(e, 'postCode');}} onClick={() => { this.showKeyboard();}} />
                              </div>
                            </th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

          <KeyboardComp />
          <div className="d-flex flex-row">
            <NavLink to={routes.APP_SCAN} className="btn btn-xxlg btn-auspostdred rounded-0" onClick={() => {this.props.touchSound();}} style={{flex: 1}}>Back</NavLink>
            <NavLink to={routes.APP_PARCELSUMMARY} className="btn btn-xxlg btn-auspostred rounded-0" onClick={() => {this.props.touchSound();}} style={{flex: 3}}>Tap to proceed</NavLink>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    showVirtualKeyboard: state.window.showVirtualKeyboard,
    parcel: state.parcel.parcel,
    delivery: state.parcel.delivery
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setTips: WindowActions.setTips,
    showHideVirtualKeyboard: WindowActions.showHideVirtualKeyboard,
    setParcelFieldValue: ParcelActions.setParcelFieldValue,
    setDeliveryFieldValue: ParcelActions.setDeliveryFieldValue,
    clearParcel: ParcelActions.clearParcel,
    touchSound: WindowActions.touchSound
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeliveryInfo);
