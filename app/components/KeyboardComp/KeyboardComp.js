import React, { Component } from 'react';
import Keyboard from 'react-simple-keyboard';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { shell, remote } from 'electron';
import { showHideVirtualKeyboard, touchSound } from '../../actions/window';

class KeyboardComp extends Component {

  constructor(props) {
    super(props);

    this.keyboardcomp = React.createRef();
    this.keyboard = React.createRef();

    this.elmnt = null;
    this.pos1 = 0;
    this.pos2 = 0;
    this.pos3 = 0;
    this.pos4 = 0;
  }

  componentDidMount = () => {
    this.dragElement(document.getElementById("keyboard-comp"));
  }

  componentDidUpdate = (prevProps) => {
    if(this.props.showVirtualKeyboard && this.props.showVirtualKeyboard !== prevProps.showVirtualKeyboard) {

      this.pos2 = '1350';
      this.pos1 = '200';

      this.elmnt.style.top = this.pos2 + "px";
      this.elmnt.style.left = this.pos1 + "px";

      this.props.touchSound();
    }
  }

  onKeyPress = (button) => {

    this.props.touchSound();

    if (button === "{shift}" || button === "{lock}") this.handleShift();

    // remote.getCurrentWindow().webContents.sendInputEvent({ type: 'char', keyCode: String.fromCharCode(65)});
    if(button === "{bksp}") { 
      remote.getCurrentWindow().webContents.sendInputEvent({ type: 'keyDown', keyCode: '\u0008'});
      remote.getCurrentWindow().webContents.sendInputEvent({ type: 'keyUp', keyCode: '\u0008'});

      try {
        document.getElementById("iframe").sendInputEvent({ type: 'keyDown', keyCode: '\u0008'});
        document.getElementById("iframe").sendInputEvent({ type: 'keyUp', keyCode: '\u0008'});
      } catch(e) {

      }
    }
    else if(button === "{space}") { 
      remote.getCurrentWindow().webContents.sendInputEvent({ type: 'char', keyCode: String.fromCharCode(32)});

      try {
        document.getElementById("iframe").sendInputEvent({ type: 'char', keyCode: String.fromCharCode(32)});
      } catch(e) {

      }
    }
    else if(button === "{shift}" || button === "{lock}") {}
    else {
      remote.getCurrentWindow().webContents.sendInputEvent({ type: 'char', keyCode: button});

      try {
        document.getElementById("iframe").sendInputEvent({ type: 'char', keyCode: button});
      } catch(e) {

      }
    }
    
  }

  handleShift = () => {

    let currentLayout = this.keyboard.current.keyboard.options.layoutName;
    let shiftToggle = currentLayout === "default" ? "shift" : "default";

    this.keyboard.current.keyboard.setOptions({
      layoutName: shiftToggle
    })
  }

  dragElement = (elmnt) => {
    this.elmnt = elmnt;
    if (document.getElementById(elmnt.id + "-header")) {
      // if present, the header is where you move the DIV from:
      // document.getElementById(elmnt.id + "-header").onmousedown = this.dragMouseDown;
      document.getElementById(elmnt.id + "-header").onpointerdown = this.dragMouseDown;
    } else {
      // otherwise, move the DIV from anywhere inside the DIV: 
      // elmnt.onmousedown = this.dragMouseDown;
      elmnt.onpointerdown = this.dragMouseDown;
    }  
  }
  
  dragMouseDown = (e) => {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    this.pos3 = e.clientX;
    this.pos4 = e.clientY;
    // document.onmouseup = this.closeDragElement;
    document.onpointerup = this.closeDragElement;
    // call a function whenever the cursor moves:
    // document.onmousemove = this.elementDrag;
    // document.onpointermove = this.elementDrag;
    document.onpointermove = this.elementDrag;
  }
  
  elementDrag = (e) => {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    this.pos1 = this.pos3 - e.clientX;
    this.pos2 = this.pos4 - e.clientY;
    this.pos3 = e.clientX;
    this.pos4 = e.clientY;
    // set the element's new position:
    this.elmnt.style.top = (this.elmnt.offsetTop - this.pos2) + "px";
    this.elmnt.style.left = (this.elmnt.offsetLeft - this.pos1) + "px";
  }
  
  closeDragElement = () => {
    // stop moving when mouse button is released:
    // document.onmouseup = null;
    // document.onmousemove = null;
    document.onpointerup = null;
    document.onpointerdown = null;
    document.onpointermove = null;
  }


  render () {
    return (
      <div id="keyboard-comp" ref={this.keyboardcomp}>
        <div style={{display: this.props.showVirtualKeyboard ? `` : `none`}}>
        <div id="keyboard-comp-header"><div className="btn btn-light btn-lg"><span className="fas fa-arrows-alt"></span></div></div>
        <div id="keyboard-comp-toggle-keyboard"><div className="btn btn-danger btn-lg" onClick={() => { this.props.showHideVirtualKeyboard(!this.props.showVirtualKeyboard); this.props.touchSound(); }}><span className="fas fa-times"></span></div></div>

          <Keyboard
            ref={this.keyboard}
            onKeyPress={(button) => { this.onKeyPress(button); }}
            preventMouseDownDefault={true}
            stopMouseDownPropagation={false}
            // autoUseTouchEvents={true}
            layout={{
              'default': [
                '1 2 3 4 5 6 7 8 9 0',
                'q w e r t y u i o p',
                'a s d f g h j k l',
                '{shift} z x c v b n m {bksp}',
                '{space}'
              ],
              'shift': [
                '1 2 3 4 5 6 7 8 9 0',
                'Q W E R T Y U I O P',
                'A S D F G H J K L',
                '{shift} Z X C V B N M {bksp}',
                '{space}'
              ]
            }}
            display={{
              '{bksp}': '<span class="fas fa-arrow-left"></span>',
              '{shift}': '<span class="fas fa-arrow-up"></span>',
              '{space}': 'space',
            }}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  showVirtualKeyboard: state.window.showVirtualKeyboard
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showHideVirtualKeyboard,
      touchSound
    },
    dispatch
  );
  

export default connect(mapStateToProps, mapDispatchToProps)(KeyboardComp);