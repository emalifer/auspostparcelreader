import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes';
import App from './components/App/App';

export default () => (
  <App>
    <Switch>
      <Route path={routes.APP} component={App} exact />
    </Switch>
  </App>
);
