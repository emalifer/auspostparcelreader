// @flow
import { parcel, SETPARCELFIELDVALUE, SETDELIVERYFIELDVALUE, CLEARPARCEL } from '../actions/parcel';
import produce from 'immer';
import type { Action } from './types';

const initialState = {
  parcel: {
    packageType: '',
    packaging: '',
    size: '',
    weight: ''
  },
  delivery: {
    company: '',
    contactName: '',
    mobileNumber: '',
    APCustomerNumber: '',
    POBoxStreet: '',
    suburbTown: '',
    state: '',
    postCode: '',
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETPARCELFIELDVALUE:
      return produce(state, draftState => {
        draftState.parcel[action.field] = action.value;
      });
    case SETDELIVERYFIELDVALUE:
      return produce(state, draftState => {
        draftState.delivery[action.field] = action.value;
      });
    case CLEARPARCEL:
      return {
        ...initialState
      }
    default:
      return state;
  }
}