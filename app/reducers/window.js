// @flow
import { SETTIPS, SHOWHIDETIPS, SHOWVIRTUALKEYBOARD, TOUCHSOUND } from '../actions/window';
import type { Action } from './types';

const initialState = {
  tips: [],
  showTips: true,
  showVirtualKeyboard: false,
  touchSound: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETTIPS:
      return {
        ...state,
        tips: action.tips
      };
    case SHOWHIDETIPS:
      return {
        ...state,
        showTips: action.show
      };
    case SHOWVIRTUALKEYBOARD:
      return {
        ...state,
        showVirtualKeyboard: action.show  
      }
    case TOUCHSOUND:
      return {
        ...state,
        touchSound: action.touchSound  
      }
    default:
      return state;
  }
}