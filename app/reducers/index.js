// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import window from './window';
import parcel from './parcel';

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    window,
    parcel
  });
}
